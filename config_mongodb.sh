#!/bin/bash
#   ....
#   ....

# Declare >>
## Variable 
cr=`echo $'\n.'`
cr=${cr%.}

## Function
comfirm() {
      while true; do
            read -p "$1 [Y]es or [n]o): " reply
            case "${reply:-Y}" in
                  [Yy]* ) return 0;;
                  [Nn]* ) return 1;;
                  * ) echo "Error: Please answer YES or NO !!!";;
            esac
      done
} 
#> End line
nechon() {
      echo "$cr$1$cr"
}
#> Install mongo
install_mongodb() {
                  nechon "========= # Installing mongod =========" 
                  #Install mongoDB community Edition on Ubuntu 22.04 (jammy)
                  #1. Import the public key used by the package management system
                  nechon "#1. Import the public key used by the package management system"
                  if which gpg >/dev/null; then 
                        nechon "#1.1. Gnupg Installed"
                  else
                        nechon "#1.1. Installing gnupg"
                        sudo apt -y install gnupg
                  fi
                  nechon "1.2. Import the MongoDB public GPG Key"
                  curl -fsSL https://pgp.mongodb.com/server-6.0.asc | \
                  sudo gpg -o /usr/share/keyrings/mongodb-server-6.0.gpg \
                  --dearmor
                  #2. Create a list file for MongoDB.
                  nechon "#2. Create a list file for MongoDB"
                  echo "deb [ arch=amd64,arm64 signed-by=/usr/share/keyrings/mongodb-server-6.0.gpg ] \
                              https://repo.mongodb.org/apt/ubuntu jammy/mongodb-org/6.0 multiverse" | \
                        sudo tee /etc/apt/sources.list.d/mongodb-org-6.0.list
                  #3. Reload local package database
                  nechon "#3. Reload local package database"
                  sudo apt-get update
                  #4. Install the MongoDB packages.
                  nechon "#4. Install the MongoDB packages latest version"
                  sudo apt install -y mongodb-org
                  #5. Start MongoDB.
                  nechon "#5. Start MongoDB"
                  sudo systemctl daemon-reload
                  sudo systemctl start mongod.service
                  sudo systemctl enable mongod.service
                  #6. Để truy cập mongdb từ xa:
                  nechon "#6. Set IP for MongoDB"
                  #6.1 Get ethernet card physical name
                  nechon "#6.1. Get ethernet card physical name"
                  eth=$(ip r | grep default | awk '/default/ {print $5}')
                  nechon $eth
                  #6.2 Get Ip address
                  nechon "#6.2. Get Ip address"
                  ip=$(ip -4 -o addr show $eth | awk '{print $4}' | cut -d "/" -f 1)
                  nechon $ip
                  #6.3 Set Ip for MongoDB 
                  nechon "#6.3. Set IP for MongoDB"
                  sudo sed -i "s/bindIp:.*/bindIp: $ip/g" /etc/mongod.conf
                  sudo systemctl restart mongod.service
}
#> Set up replica Primary
setup_replica_primary() {
      if comfirm "Do you want to config Replica Set now ?"; then
            nechon "======== #7 Setup Replica Set is in progress ========"
            read -p "#7.1. Enter replSetname: " replsetname
            # Key file path
            fileKeyPath='/var/lib/mongodb/mongokey'
            [ -f "$fileKeyPath" ] && nechon $fileKeyPath || touch $fileKeyPath
            # Create key file 
            nechon "#7.2. Create key file"
            openssl rand -base64 756 > $fileKeyPath
            sudo chmod 400 $fileKeyPath
            sudo chown -R mongodb:mongodb $fileKeyPath
            # Copy key file to secondary server
            if comfirm "Copy KEY FILE to Secondary node server ? (Secondary must be running)"; then
            read -p "${cr}#7.3. Enter the number of Secondary server: " numberserver
            for (( i=1; i<=$numberserver; i++ ));
            do 
                  read -p "${cr}Enter IP Secondary Server $i: " ipsv
                  read -p "${cr}Enter Username Secondary Server $i: " usv 
                  sudo scp $fileKeyPath $usv@$ipsv:/home/$usv && nechon "Copy to Server successfully" || nechon "Copy to Server Fails"
            done
            fi
            # Edit /etc/mongod.conf
            nechon "#7.4. Config mongod ..."
            sudo sed -i "s|#security:|security:\n  keyFile: $fileKeyPath|g" /etc/mongod.conf
            sudo sed -i "s/#replication:/replication:\n  replSetName: '$replsetname'/g" /etc/mongod.conf
            sudo systemctl restart mongod.service
      else
            nechon "Okay, exit" 
      fi
}
#> Set up replica Secondary
setup_replica_secondary() {
      if comfirm "Do you want to config Replica Set now ?"; then
            nechon "======== # Setup Replica Set is in progress ========"
            read -p "#1. Enter replSetname: " replsetname
            # Key file path
            current_user=$(pwd)
            key_file_name=mongokey
            key_file_source=${current_user}/$key_file_name
            key_file_path='/var/lib/mongodb/'
            if [ -f "$key_file_source" ]; then
                  sudo chmod 777 $key_file_source 
                  sudo cp -n $key_file_source $key_file_path
                  sudo chmod 400 $key_file_path$key_file_name
                   sudo chown -R mongodb:mongodb $key_file_path$key_file_name
            else 
                  nechon "Error: Key File not found !!!"            
            fi
            # edit /etc/mongod.conf
            nechon "#2. Config mongod ..."
            sudo sed -i "s|#security:|security:\n  keyFile: $key_file_path$key_file_name|g" /etc/mongod.conf
            sudo sed -i "s/#replication:/replication:\n  replSetName: '$replsetname'/g" /etc/mongod.conf
            sudo systemctl restart mongod.service
      else
            nechon "Okay, exit" 
      fi
}
# << Declare
# ====================================================================================================================
# Main >>
## Dam bao rang chi co user root moi duoc thuc thi script
if [ "$EUID" -ne 0 ]; then
      nechon "Error: This script must be run as root !!!"
      exit
fi


## Choose option 
primary_node='[1]. Install Mongodb primary node'
secondary_node='[2]. Install Mongodb secondary node'
read -p "${cr}Please choose option to install: ${cr}$primary_node ${cr}$secondary_node${cr}Your choice: "  option
      case "${option}" in
            [1] )
# ========================================== Install Mongodb Primary node ============================================
            nechon "====== Install Mongodb primary node ======"
            ## Check mongodb
            if which mongod >/dev/null
            then 
                  nechon "|||||||||||| MONGODB IS ALREADY INSTALLED |||||||||||"
                  ## Set up Replica Primary
                  setup_replica_primary
            else
                  ## Install mongodb
                  install_mongodb
                  ## 6.4 Create account MongoDB Admin 
                  if comfirm "Create Admin MongoDB Account now? "
                  then
                        nechon "#6.4. Create account MongoDB Admin"
                        read -p "${cr}Enter username: " username
                        read -sp "${cr}Enter password: " password
                        nechon
                  ##  
                  sudo mongosh --host $ip <<EOF
                  use admin
                  db.createUser(
                        {          
                              user: "$username",
                              pwd: "$password",
                              roles: [ { role: 'root', db: 'admin' } ]
                        }
                  );
EOF
                  nechon
                  else exit
                  fi
                  ## Set up replica Primary
                  setup_replica_primary      
            fi ;;
            [2] )
# ========================================== Install Mongodb Secondary node ============================================
            nechon "====== Install Mongodb Secondary node ======" 
            ## Check mongodb
            if which mongod >/dev/null
            then 
                  nechon "||||||||||| MONGODB IS ALREADY INSTALLED |||||||||||"
                  ## Set up Replica Secondary
                  setup_replica_secondary
            else 
                  ## Install mongodb
                  install_mongodb
                  ## Set up Replica Secondary
                  setup_replica_secondary
            fi ;;
            * ) echo "Error: Please answer 1 or 2 !!!";;
      esac
## Restart mongodb service
sudo systemctl restart mongod.service
if  sudo systemctl status mongod | grep -qw active
then
      echo "|||||||||||||||| MONGODB IS RUNNING |||||||||||||||||"
      echo -e "||||||||||| HAPPYYY 凸(¬‿¬)凸 SUCCESSFULL ||||||||||| \n"
else
      nechon "MongoDB is starting...\n"
      sudo systemctl start mongod.service
      systemctl status mongod.service
fi

# << Main